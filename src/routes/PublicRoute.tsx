import React from "react";
import {Route, Redirect} from "react-router-dom";
import {useSelector} from "react-redux";
import {IAppState} from "../redux/Types";

export function PublicRoute({component: Component, ...rest}) {
   const isAuthorized = useSelector<IAppState>(state => state.login.isAuthorized);
   return (
      <Route
         {...rest}
         render={props => (isAuthorized ? <Redirect to="/todo" /> : <Component {...props} />)}
      />
   );
}
