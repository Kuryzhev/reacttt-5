import React from "react";
import {Router, Switch, Redirect} from "react-router-dom";
import Login from "../componets/Login";
import NavigationBar from "../componets/NavigationBar";
import ItemsList from "../componets/ItemsList";
import {ProtectedRoute} from "./ProtectedRoute";
import {PublicRoute} from "./PublicRoute";
import {history} from "./Browserhistory";
import ItemDetails from "../componets/ItemDetails";

export const RootRoute = () => {
   return (
      <Router history={history}>
         <div>
            <NavigationBar />
            <Switch>
               <PublicRoute path="/login" component={Login} />
               <ProtectedRoute path="/todo" component={ItemsList} />
               <ProtectedRoute path="/todo/:id" component={ItemDetails} />
               <Redirect from="/" to="/login" />
            </Switch>
         </div>
      </Router>
   );
};
