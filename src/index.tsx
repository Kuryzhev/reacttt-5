import * as React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import registerServiceWorker from "./service/registerServiceWorker";

ReactDOM.render(<App />, document.getElementById("root"));
registerServiceWorker();
