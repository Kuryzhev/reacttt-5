import * as React from "react";
import {IAppState} from "../redux/Types";
import {connect, ConnectedProps} from "react-redux";
import {Link} from "react-router-dom";
import "../styles/NavigationBar.css";

const putStateToProps = (state: IAppState) => {
   const {isAuthorized} = state.login;
   return {isAuthorized};
};
const connector = connect(putStateToProps);
type PropsFromRedux = ConnectedProps<typeof connector>;

function NavigationBar(props: PropsFromRedux) {
   const {isAuthorized} = props;
   const authorized = () => {
      return (
         <>
            <Link to="/Items" className="link">
               Items List
            </Link>
         </>
      );
   };

   return <div className="naviagtion-bar">{isAuthorized && authorized()}</div>;
}

export default connector(NavigationBar);
