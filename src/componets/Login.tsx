import * as React from "react";
import {IAppState} from "../redux/Types";
import {connect, ConnectedProps} from "react-redux";
import {AUTH_ACTIONS} from "../redux/actions/AuthActionCreators";
import "../styles/Login.css";

const putStateToProps = (state: IAppState) => {
   const {isAuthorized, username} = state.login;
   return {isAuthorized, username};
};

const putActionsToProps = {
   onSubmit: username => AUTH_ACTIONS.loginRequest(username),
};

const connector = connect(putStateToProps, putActionsToProps);
type PropsFromRedux = ConnectedProps<typeof connector>;

type IState = {
   username: string;
};

class Login extends React.PureComponent<PropsFromRedux, IState> {
   constructor(parameters: PropsFromRedux) {
      super(parameters);

      this.state = {username: ""};
   }

   _onChangeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
      const {value} = event.target;
      this.setState({username: value});
   };

   _onSubmitHandler = (event: React.FormEvent<HTMLFormElement>) => {
      const {username} = this.state;
      const {onSubmit} = this.props;
      //  event.preventDefault();
      if (username !== "") onSubmit(username);
   };

   render() {
      const {isAuthorized, username} = this.props;

      return (
         <div className="login-wrap">
            <h2 className="title">Login</h2>
            <form name="form" onSubmit={this._onSubmitHandler}>
               <div className="login-section">
                  <label>Username:</label>
                  <input type="text" name="username" onChange={this._onChangeHandler} />
               </div>
               <button type="submit">Sign In</button>
            </form>
         </div>
      );
   }
}
export default connector(Login);
