import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {IAppState} from "../redux/Types";
import {ITEMS_LIST_ACTIONS} from "../redux/actions/ItemsActionCreators";
import {IUserItem} from "../interfaces/IUserItem";
import {REQUEST_PATH} from "../service/RequestPath";

function ItemDetails() {
   const dispatch = useDispatch();
   const details = useSelector<IAppState, IUserItem>(state => state.currentItem.details);

   useEffect(() => {
      fetch(REQUEST_PATH + details.id)
         .then(response => response.json())
         .then(response => {
            dispatch(ITEMS_LIST_ACTIONS.getItemsSuccess(response));
         })
         .catch(error => {
            dispatch(ITEMS_LIST_ACTIONS.getItemsFailure(error));
         });
   }, [dispatch]);

   return (
      <div>
         <div className="details">
            {details && (
               <div>
                  <p>Title: {details.title}</p>
                  <p>User ID: {details.userId}</p>
                  <p>ID: {details.id}</p>
                  <p className={details.completed ? "completed" : "not-completed"}>
                     Status: {details.completed ? "Completed" : "Not completed"}
                  </p>
               </div>
            )}
         </div>
      </div>
   );
}
export default ItemDetails;
