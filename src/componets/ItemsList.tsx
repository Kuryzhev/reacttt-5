import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {IAppState, InitialStateItems} from "../redux/Types";
import {ItemComponent} from "./ItemComponent";
import {ITEMS_LIST_ACTIONS} from "../redux/actions/ItemsActionCreators";
import {IUserItem} from "../interfaces/IUserItem";
import "../styles/ItemsList.css"
import { REQUEST_PATH } from '../service/RequestPath';

function ItemsList() {
   const dispatch = useDispatch();
   const items = useSelector<IAppState, Array<IUserItem>>(state => state.list.items);

   useEffect(() => {
      fetch(REQUEST_PATH)
         .then(response => response.json())
         .then(response => {
            dispatch(ITEMS_LIST_ACTIONS.getItemsSuccess(response));
         })
         .catch(error => {
            dispatch(ITEMS_LIST_ACTIONS.getItemsFailure(error));
         });
   }, [dispatch]);

   return <div className="items-holder">{items && items.map(item => <ItemComponent key={item.id} {...item} />)}</div>;
}
export default ItemsList;
