import React from "react";
import {IUserItem} from "../interfaces/IUserItem";
import {history} from "../routes/Browserhistory";

interface IProps extends IUserItem {}

export function ItemComponent(props: IProps) {
   const {id, title} = props;
   return (
      <div key={props.id} className="item">
         <p>Title: {title}</p>
         <button className="button" onClick={() => history.push(`/todo/${id}`)}>
            Details
         </button>
      </div>
   );
}
