import {combineReducers, createStore} from "redux";
import {IAppState} from "../Types";
import loginReducer from "../reducers/LoginReducer";
import ItemsReduser from "../reducers/ItemsReduser";
import ItemDetailsReduser from "../reducers/ItemDetailsReduser";

const store = createStore<IAppState, any, any, any>(
   combineReducers({
      login: loginReducer,
      list: ItemsReduser,
      currentItem: ItemDetailsReduser,
   }),
);
export default store;
