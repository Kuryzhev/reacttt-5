import {AUTHENTICATION} from "../Consts";
import {InitialStateLogin} from "../Types";

const initialState: InitialStateLogin = {
   username: "",
   isAuthorized: false,
};

export default (state = initialState, action: any): InitialStateLogin => {
   switch (action.type) {
      case AUTHENTICATION.LOGIN_REQUEST:
         return {...state, username: action.payload, isAuthorized: true};
      case AUTHENTICATION.LOGOUT_SUCCESS:
         return {...state, username: "", isAuthorized: false};
      default:
         return {...state};
   }
};
