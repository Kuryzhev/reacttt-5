import * as React from "react";
import {ITEMS} from "../Consts";
import {InitialDetailsItem} from "../Types";
import {IUserItem} from "../../interfaces/IUserItem";

const initialState: InitialDetailsItem = {
   details: <IUserItem>{
      userId: 0,
      id: 0,
      title: "",
      completed: false,
   }
};

export default (state = initialState, action): InitialDetailsItem => {
   switch (action.type) {
      case ITEMS.ITEMS_LIST_SUCCESS:
         return {...state, details: action.payload};
      case ITEMS.ITEMS_LIST_FAILURE:
         return {...state, error: "Error"};
      default:
         return {...state};
   }
};
