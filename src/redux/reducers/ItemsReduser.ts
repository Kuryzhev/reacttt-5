import {ITEMS} from "../Consts";
import {InitialStateItems} from "../Types";

const initialState: InitialStateItems = {
   items: [],
};

export default (state = initialState, action): InitialStateItems => {
   switch (action.type) {
      case ITEMS.ITEMS_LIST_SUCCESS:
         return {...state, items: action.payload};
      case ITEMS.ITEMS_LIST_FAILURE:
         return {...state, error: "Error"};
      default:
         return {...state};
   }
};
