import {AUTHENTICATION} from "../Consts";

export const AUTH_ACTIONS = {
   loginRequest,
   loginSuccess,
   loginFailure,
};

function loginRequest(username: string) {
   return {
      type: AUTHENTICATION.LOGIN_REQUEST,
      payload: username,
   };
}

function loginSuccess(username: string) {
   return {
      type: AUTHENTICATION.LOGIN_SUCCESS,
      payload: username,
   };
}

function loginFailure(e: Error) {
   return {
      type: AUTHENTICATION.LOGIN_FAILURE,
      payload: e,
   };
}
