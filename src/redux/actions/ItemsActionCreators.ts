import {ITEMS} from "../Consts";
import {IUserItem} from "../../interfaces/IUserItem";

export const ITEMS_LIST_ACTIONS = {
   getItemsSuccess,
   getItemsFailure,
   getItemDetailsSuccess,
   getItemsDetailsFailure,
};

function getItemsSuccess(items: Array<IUserItem>) {
   return {
      type: ITEMS.ITEMS_LIST_SUCCESS,
      payload: items,
   };
}

function getItemsFailure(e) {
   return {
      type: ITEMS.ITEMS_LIST_FAILURE,
      payload: e,
   };
}

function getItemDetailsSuccess(item: IUserItem) {
   return {
      type: ITEMS.ITEMS_LIST_SUCCESS,
      payload: item,
   };
}

function getItemsDetailsFailure(e) {
   return {
      type: ITEMS.ITEMS_LIST_FAILURE,
      payload: e,
   };
}
