import {IUserItem} from "../interfaces/IUserItem";

export type InitialStateLogin = {
   username: string;
   isAuthorized: Boolean;
};

export interface InitialStateItems {
   items: IUserItem[];
   error?: string;
}
export interface InitialDetailsItem {
   details: IUserItem;
   error?: string;
}

export interface IAppState {
   login: InitialStateLogin;
   list: InitialStateItems;
   currentItem: InitialDetailsItem;
}
