import React from "react";
import {Provider} from "react-redux";
import store from "./redux/store/Store";
import {RootRoute} from "./routes/RootRoute";

const App: React.FC<{}> = () => {
   return (
      <Provider store={store}>
         <RootRoute />
      </Provider>
      // <div>
      //    {service.status === "loading" && <div>Loading...</div>}
      //    {service.status === "loaded" &&
      //       service.payload.map(item => <div key={item.id}>{item.userId}</div>)}
      //    {service.status === "error" && <div>Error, the backend moved to the dark side.</div>}
      // </div>
   );
};

export default App;
